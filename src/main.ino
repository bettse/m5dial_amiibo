#include "M5Dial.h"
#include <SPIFFS.h>
extern "C" {
#include "nfc3d/amiibo.h"
}

#define HowBigIsThisArray(x) (sizeof(x) / sizeof(x[0]))

#define Serial USBSerial
#define NTAG215_SIZE 540
#define NFC3D_UID_OFFSET 0x1D4
#define PAGE_SIZE 4

long oldPosition = -999;
nfc3d_amiibo_keys amiiboKeys;

long previousMillis = 0;
long tick = 1000;
long idle = 0; // tick count
#define IDLE_TIMEOUT 5 * 60

uint8_t original[NTAG215_SIZE];
uint8_t plain_base[NFC3D_AMIIBO_SIZE];
uint8_t modified[NTAG215_SIZE];

uint8_t SLB[] = {0x00, 0x00, 0x0F, 0xE0};
uint8_t CC[] = {0xf1, 0x10, 0xff, 0xee};
uint8_t DLB[] = {0x01, 0x00, 0x0f, 0xbd};
uint8_t CFG0[] = {0x00, 0x00, 0x00, 0x04};
uint8_t CFG1[] = {0x5f, 0x00, 0x00, 0x00};
uint8_t PACKRFUI[] = {0x80, 0x80, 0x00, 0x00};

enum NTAG215Pages {
  staticLockBits = 2,
  capabilityContainer = 3,
  userMemoryFirst = 4,
  userMemoryLast = 129,
  dynamicLockBits = 130,
  cfg0 = 131,
  cfg1 = 132,
  pwd = 133,
  pack = 134,
  total = 135
};

void calculate_pwd(uint8_t *uid, uint8_t *pwd) {
  pwd[0] = uid[1] ^ uid[3] ^ 0xAA;
  pwd[1] = uid[2] ^ uid[4] ^ 0x55;
  pwd[2] = uid[3] ^ uid[5] ^ 0xAA;
  pwd[3] = uid[4] ^ uid[6] ^ 0x55;
}

void nfc_error() {
  M5Dial.Display.clear();
  M5Dial.Display.drawString("error", M5Dial.Display.width() / 2,
                            M5Dial.Display.height() / 2 - 30);
  M5Dial.Rfid.PICC_HaltA();
  M5Dial.Speaker.tone(8000, 20);
  delay(20);
  M5Dial.Speaker.tone(4000, 20);
  delay(1000);
}

void setup() {
  auto cfg = M5.config();
  Serial.begin(115200);
  M5Dial.begin(cfg, true, true);
  M5Dial.Display.setRotation(2);
  M5Dial.Display.setTextColor(GREEN);
  M5Dial.Display.setTextDatum(middle_center);
  M5Dial.Display.setTextFont(&fonts::Orbitron_Light_32);
  M5Dial.Display.setTextSize(1);

  M5Dial.Display.drawString("Maker", M5Dial.Display.width() / 2,
                            M5Dial.Display.height() / 2);
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  File keys = SPIFFS.open("/key_retail.bin");
  keys.read((uint8_t *)&amiiboKeys, sizeof(amiiboKeys));
  keys.close();
}

void listFiles() {
  File root = SPIFFS.open("/");
  File file = root.openNextFile();
  while (file) {
    Serial.print("FILE: ");
    Serial.println(file.name());
    file = root.openNextFile();
  }

  file.close();
  root.close();
}

File getNthFile(uint8_t n) {
  Serial.printf("getNthFile %d\n", n);
  File root = SPIFFS.open("/a");
  File file;
  do {
    file = root.openNextFile();
    if (!file) {
      return file;
    }
  } while (n-- > 0);
  root.close();

  return file;
}

void loop() {
  M5Dial.update();

  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > tick) {
    previousMillis = currentMillis;

    idle++;

/*
    Serial.printf("USB: %i\n", M5Dial.Power.getUsbOutput());
    Serial.printf("Power %i %i, %i\n", M5Dial.Power.getBatteryLevel(), M5Dial.Power.isCharging(), M5Dial.Power.getBatteryVoltage());
*/

    if (idle > IDLE_TIMEOUT) {
      idle = 0;
      gpio_wakeup_enable(GPIO_NUM_42, GPIO_INTR_LOW_LEVEL);
      esp_sleep_enable_gpio_wakeup();
      esp_light_sleep_start();

      // Requires button on button to wake up
      // M5Dial.Power.powerOff();
    }
  }

#define TICK_PER_CLICK 4
  long newPosition = M5Dial.Encoder.read() / TICK_PER_CLICK;
  if (newPosition < 0) {
    newPosition = 0;
    M5Dial.Encoder.write(newPosition);
  }
  if (newPosition != oldPosition) {
    idle = 0;
    M5Dial.Speaker.tone(4000, 20);
    oldPosition = newPosition;
    Serial.println(newPosition);
    M5Dial.Display.clear();
    M5Dial.Display.drawString("Loading...", M5Dial.Display.width() / 2,
                              M5Dial.Display.height() / 2);

    File f = getNthFile(newPosition);
    M5Dial.Display.clear();
    if (f) {
      String name = f.name();
      int ext = name.lastIndexOf('.');
      name.remove(ext);

      int index = name.indexOf(' ');
      if (index == -1) { // No space found
        M5Dial.Display.drawString(name, M5Dial.Display.width() / 2,
                                  M5Dial.Display.height() / 2);
      } else {
        int next = name.indexOf(' ', index + 1);
        if (next == -1) { // 1 space found
          M5Dial.Display.drawString(name.substring(0, index),
                                    M5Dial.Display.width() / 2,
                                    M5Dial.Display.height() / 2);
          M5Dial.Display.drawString(name.substring(index),
                                    M5Dial.Display.width() / 2,
                                    M5Dial.Display.height() / 2 + 30);
        } else { // 2+ spaces
          M5Dial.Display.drawString(name.substring(0, index),
                                    M5Dial.Display.width() / 2,
                                    M5Dial.Display.height() / 2 - 30);
          M5Dial.Display.drawString(name.substring(index, next),
                                    M5Dial.Display.width() / 2,
                                    M5Dial.Display.height() / 2);
          M5Dial.Display.drawString(name.substring(next),
                                    M5Dial.Display.width() / 2,
                                    M5Dial.Display.height() / 2 + 30);
        }
      }

      f.read((uint8_t *)&original, NTAG215_SIZE);
      f.close();

      if (!nfc3d_amiibo_unpack(&amiiboKeys, original, plain_base)) {
        Serial.printf("!!! WARNING !!!: Tag signature was NOT valid\n");
        return nfc_error();
      }
    } else {
      String msg = String(newPosition) + ": Empty";
      M5Dial.Display.drawString(msg, M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
    }
  }

  if (M5Dial.Rfid.PICC_IsNewCardPresent() &&
      M5Dial.Rfid.PICC_ReadCardSerial()) {
    idle = 0;
    M5Dial.Display.clear();

    Serial.print(F("PICC type: "));
    uint8_t piccType = M5Dial.Rfid.PICC_GetType(M5Dial.Rfid.uid.sak);
    Serial.println(M5Dial.Rfid.PICC_GetTypeName(piccType));

    String uid = "";
    for (byte i = 0; i < M5Dial.Rfid.uid.size; i++) {
      Serial.printf("%02X ", M5Dial.Rfid.uid.uidByte[i]);
      uid += String(M5Dial.Rfid.uid.uidByte[i], HEX);
    }
    Serial.println();

    if (piccType == MFRC522::PICC_TYPE_MIFARE_UL) {
      uint8_t ret;
      uint8_t block[32];
      uint8_t len = 32;

      if (M5Dial.Rfid.uid.size != 7) {
        Serial.printf("UID Length error\n");
        return nfc_error();
      }

      ret = M5Dial.Rfid.MIFARE_Read(0, block, &len);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("Read error: %d\n", ret);
        return nfc_error();
      }

      Serial.print("Block returned: ");
      for (uint8_t i = 0; i < len; i++) {
        Serial.printf("%02X ", block[i]);
      }
      Serial.println();

      uint8_t *cc = block + 12;
      if (cc[2] == 0x3E) {
        Serial.println("NTAG215");
        M5Dial.Display.drawString("NTAG215", M5Dial.Display.width() / 2,
                                  M5Dial.Display.height() / 2 - 30);
        M5Dial.Speaker.tone(4000, 20);
      } else {
        Serial.printf("Not NTAG215.  cc[2] = %02x\n", cc[2]);
        return nfc_error();
      }

      // UID + BCC
      for (size_t i = 0; i < PAGE_SIZE * 2; i++) {
        plain_base[NFC3D_UID_OFFSET + i] = block[i];
      }

      nfc3d_amiibo_pack(&amiiboKeys, plain_base, modified);

      Serial.println("Debug:");
      for (size_t i = 0; i < NTAG215_SIZE; i++) {
        Serial.printf("%02X ", modified[i]);
      }
      Serial.println();

      // user data
      // pwd
      // pack
      // capability container
      // cfg0
      // cfg1
      // dynamic lock bits
      // static lock bits

      for (uint8_t i = userMemoryFirst; i <= userMemoryLast; i++) {
        uint8_t *page_data = modified + (PAGE_SIZE * i);
        ret = M5Dial.Rfid.MIFARE_Ultralight_Write(i, page_data, PAGE_SIZE);
        if (ret != MFRC522::STATUS_OK) {
          Serial.printf("[Page %d] Write error: %d\n", i, ret);
          if (ret == MFRC522::STATUS_TIMEOUT) {
            M5Dial.Display.drawString("NFC Timeout", M5Dial.Display.width() / 2,
                                      M5Dial.Display.height() / 2);

            delay(1000);
            return;
          } else {
            return nfc_error();
          }
        }

        M5Dial.Display.clear();

        String msg = "Page " + String(i);
        M5Dial.Display.drawString(msg, M5Dial.Display.width() / 2,
                                  M5Dial.Display.height() / 2);
        M5Dial.Speaker.tone(2000 + 50 * i, 20);
      }

      uint8_t PWD[PAGE_SIZE];
      calculate_pwd(M5Dial.Rfid.uid.uidByte, PWD);
      ret = M5Dial.Rfid.MIFARE_Ultralight_Write(pwd, PWD, PAGE_SIZE);
      M5Dial.Display.drawString("PWD", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("[PWD] Write error: %d\n", ret);
        return nfc_error();
      }

      M5Dial.Display.drawString("PACK", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
      ret = M5Dial.Rfid.MIFARE_Ultralight_Write(pack, PACKRFUI, PAGE_SIZE);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("[PACKRFUI] Write error: %d\n", ret);
        return nfc_error();
      }

      M5Dial.Display.drawString("CC", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
      ret = M5Dial.Rfid.MIFARE_Ultralight_Write(capabilityContainer, CC,
                                                PAGE_SIZE);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("[CC] Write error: %d\n", ret);
        return nfc_error();
      }

      M5Dial.Display.drawString("CFG0", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
      ret = M5Dial.Rfid.MIFARE_Ultralight_Write(cfg0, CFG0, PAGE_SIZE);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("[CFG0] Write error: %d\n", ret);
        return nfc_error();
      }

      M5Dial.Display.drawString("CFG1", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
      ret = M5Dial.Rfid.MIFARE_Ultralight_Write(cfg1, CFG1, PAGE_SIZE);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("[CFG1] Write error: %d\n", ret);
        return nfc_error();
      }

      M5Dial.Display.drawString("Dynamic Lock Bits", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
      ret =
          M5Dial.Rfid.MIFARE_Ultralight_Write(dynamicLockBits, DLB, PAGE_SIZE);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("[DLB] Write error: %d\n", ret);
        return nfc_error();
      }

      M5Dial.Display.drawString("Static Lock Bits", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);
      ret = M5Dial.Rfid.MIFARE_Ultralight_Write(staticLockBits, SLB, PAGE_SIZE);
      if (ret != MFRC522::STATUS_OK) {
        Serial.printf("[SLB] Write error: %d\n", ret);
        return nfc_error();
      }

      for (uint8_t i = 0; i < 3; i++) {
        M5Dial.Speaker.tone(9000 + 1000 * i, 100);
      }

      M5Dial.Display.clear();
      M5Dial.Display.drawString("Done", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);

      M5Dial.Rfid.PICC_HaltA();
    } else {
      Serial.println("Not MIFARE UL");
      M5Dial.Display.drawString("Wrong kind", M5Dial.Display.width() / 2,
                                M5Dial.Display.height() / 2);

      M5Dial.Speaker.tone(3000, 40);
      M5Dial.Speaker.tone(1000, 40);
    }
  }
}
